import com.cedarsoftware.util.io.JsonWriter;
import com.sun.net.httpserver.*;
import com.sun.org.apache.xpath.internal.operations.Bool;
import jdk.nashorn.internal.ir.debug.JSONWriter;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

// based on http://www.cs.put.poznan.pl/tpawlak/files/TPSI/TPSIServer.java
public class TPSIServer {
	public static void main(String[] args) throws Exception {
		int port = 8000;
		HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
		server.createContext("/", new RootHandler());
		server.createContext("/echo", new EchoHandler());
		server.createContext("/redirect", new RedirectHandler());
		server.createContext("/cookies", new CookiesHandler());
		server.createContext("/auth", new AuthHandler());
		server.createContext("/auth2", new Auth2Handler()).setAuthenticator(new BasicAuthenticator("auth2") {
			@Override
			public boolean checkCredentials(String user, String pwd) {
				return user.equals("user") && pwd.equals("password");
			}
		});
		System.out.println("Starting server on port: " + port);
		server.start();
	}

	static class RootHandler implements HttpHandler {
		public void handle(HttpExchange exchange) throws IOException {
			Path path = Paths.get("doc.html");
			byte[] data = Files.readAllBytes(path);

			exchange.getResponseHeaders().set("Content-Type", "text/html; charset=UTF-8");
			exchange.sendResponseHeaders(200, data.length);
			OutputStream os = exchange.getResponseBody();
			os.write(data);
			os.close();
		}
	}

	static class EchoHandler implements HttpHandler {
		public void handle(HttpExchange exchange) throws IOException {
			System.out.println("Request /echo");
			Headers headers = exchange.getRequestHeaders();
			Map args = new HashMap();
			args.put(JsonWriter.PRETTY_PRINT, true);
			String response = JsonWriter.objectToJson(headers, args);
			exchange.getResponseHeaders().set("Content-Type", "application/json");
			exchange.sendResponseHeaders(200, response.length());
			OutputStream os = exchange.getResponseBody();
			os.write(response.getBytes());
			os.close();
		}
	}

	static class RedirectHandler implements HttpHandler {
		public void handle(HttpExchange exchange) throws IOException {
			//String response = JsonWriter.objectToJson(headers);
			exchange.getResponseHeaders().set("Location", "/echo");
			System.out.println("Request /redirect");
			exchange.sendResponseHeaders(302, 0);
			OutputStream os = exchange.getResponseBody();
			//os.write(response.getBytes());
			os.close();
		}
	}

	static class CookiesHandler implements HttpHandler {
		public void handle(HttpExchange exchange) throws IOException {
			System.out.println("Request /cookies");

			Path path = Paths.get("doc.html");
			byte[] data = Files.readAllBytes(path);

			Random rand = new Random();
			int cookieId = rand.nextInt(10000) + 1;
			System.out.println(cookieId);
			exchange.getResponseHeaders().set("Content-Type", "text/html; charset=utf-8");
			exchange.getResponseHeaders().set("Set-Cookie", "Cookie="+cookieId+"; domain=localhost; path=/echo; Max-Age=86400");

			exchange.sendResponseHeaders(200, data.length);
			OutputStream os = exchange.getResponseBody();
			os.write(data);
			os.close();
		}
	}

	static class AuthHandler implements HttpHandler {
		public void handle(HttpExchange exchange) throws IOException {
			Path path = Paths.get("doc.html");
			byte[] data = Files.readAllBytes(path);
			String username = "user";
			String password = "password";
			exchange.getResponseHeaders().set("Content-Type", "text/html; charset=UTF-8");

			Headers headers = exchange.getRequestHeaders();
			Map args = new HashMap();
			args.put(JsonWriter.PRETTY_PRINT, true);
			String response = JsonWriter.objectToJson(headers, args);
			//System.out.println(response);

			//System.out.println(headers.get("Authorization"));
			List headerValue = headers.get("Authorization");
			//System.out.println(headerValue);
			String authdata = "";
			if(headerValue != null) {
				for (int i = 0; i < headerValue.size(); i++)
					authdata = headerValue.get(i).toString();
			}

			String passedUsername = "";
			String passedPassword = "";
			if(!authdata.equals("")) {
				String parts[] = authdata.split(" ");
				authdata = parts[1];
				System.out.println(authdata);
				byte bytes[] = Base64.getDecoder().decode(authdata);
				authdata = new String(bytes, "UTF-8");
				System.out.println(authdata);
				parts = authdata.split(":");
				passedUsername = parts[0];
				passedPassword = parts[1];
			}

			if(passedUsername.equals(username) && passedPassword.equals(password)) {
				exchange.getResponseHeaders().set("Content-Type", "text/html; charset=UTF-8");
				exchange.sendResponseHeaders(200, data.length);
				OutputStream os = exchange.getResponseBody();
				os.write(data);
				os.close();
			}
			else {
				exchange.getResponseHeaders().set("WWW-Authenticate", "Basic");
				exchange.sendResponseHeaders(401, 0);
				OutputStream os = exchange.getResponseBody();
				//os.write(data);
				os.close();
			}
		}
	}//AuthHandler

	static class Auth2Handler implements HttpHandler {
		public void handle(HttpExchange exchange) throws IOException {
			Path path = Paths.get("doc.html");
			byte[] data = Files.readAllBytes(path);

			exchange.getResponseHeaders().set("Content-Type", "text/html; charset=UTF-8");
			exchange.sendResponseHeaders(200, data.length);
			OutputStream os = exchange.getResponseBody();
			os.write(data);
			os.close();
		}
	}
}
